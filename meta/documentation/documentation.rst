
.. _bokertov_meta_infos:

=====================
Meta doc
=====================

.. seealso::

   - https://judaisme.gitlab.io/bokertov/

.. contents::
   :depth: 3


Gitlab project
=================

.. seealso::

   - https://gitlab.com/judaisme/bokertov/


Issues
---------

.. seealso::

   - https://gitlab.com/judaisme/bokertov/-/boards


Pipelines
--------------

.. seealso::

   - https://gitlab.com/judaisme/bokertov/-/pipelines


Sphinx theme : **sphinx_book_theme**
============================================

.. seealso::

   - https://gdevops.gitlab.io/tuto_documentation/doc_generators/sphinx/themes/sphinx_book_theme/sphinx_book_theme.html


::

    import sphinx
    liste_full = [
        "globaltoc.html",
    html_theme = "sphinx_book_theme"
    copyright = f"2018-{now.year}, {author}, Creative Commons CC BY-NC-SA 3.0. Built with sphinx {sphinx.__version__} Python {platform.python_version()} {html_theme=}"


pyproject.toml
=================

.. literalinclude:: ../../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../../Makefile
   :linenos:
