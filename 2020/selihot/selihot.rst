
.. _bokertov_selihot_2020:

============================================
Boker Tov Selihot 2020
============================================

.. seealso::

 - https://rabbinchinsky.fr/2020/09/13/boker-tov-selihot/


.. contents::
   :depth: 3


.. figure:: shofar.jpg
   :align: center


.. _13_attributs:

13 Attributs Ex. 34 :6 (et Nb 14 :18)
======================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Treize_Attributs_de_Dieu

Adonaï Adonaï el raHoum véHanoun EreH apaim vérav Héssed véémet, notser
Héssed laalafim, nossé avon vafécha véHata vénaké.

Références
-----------

- :ref:`17_septembre_2020`
- :ref:`23_septembre_2020`


.. _selihot_vidoui:

Vidouï
========


Elohénou vélohé avoténou ana tavo léfanéHa téfilaténou veal titalam
mitHinaténou chein anaHnou azé fanim oukéché orèf lomar léfanéha adonaï
élohénou vélohé avoténou tsadikim anaHnou vélo Hatanou aval anaHnou Hatanou
Achamnou bagadnou gazalnou dibarnou dofi héévinou vehirchanou zadnou
Hamasnou tafalnou chéker yaatsnou ra kizavnou lasnou maradnou niatsnou
sararnou avinou pachanou tsararnou kichinou oref rachaanou chiHatnou
tiavnou tainou titanou


Références
------------

.. seealso::

   :ref:`16_septembre_2020`



.. _adon_hasselihot:

Adon hasseliHot
=================

**Chatanu lefaneicha rachem aleinu**.

Adon haselichot, bochen levavot, goleh amukot, dover tzedakot
Hadur benifla’ot, vatik benechamot, zocher b’rit amo, choker kelayot
Male zakiyut, nora tehinot, tzone’ach avonot, oneh be’etzavot


.. _shomer_israel:

Shomer Israël
================

- ((Chomer \*2) Israël  Chmor chéérit israël \*2) /// (veal yovad \*2) Israël haomrim béHol yom chéma Israël
- ((Chomer \*2) Goy éHad Chmor chéérit Goy éHad \*2) ///(veal yovad \*2) Goy éHad haomrim béHol yom adonaï élohénou adonaï éHad
- ((Chomer \*2) Goy Kadoch Chmor chéérit Goy Kadoch \*2)/// (veal yovad \*2) Goy Kadoch haomrim béHol yom kadoch kadoch kadoch
- ((Chomer \*2) Goy Rabba  Chmor chéérit Goy Rabba \*2) ///(veal yovad \*2) Goy Rabba haomrim béHol yom amen yéhé chémé rabba
