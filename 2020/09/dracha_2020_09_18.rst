
=======================================================================================
Vendredi 18 septembre 2020 **Dracha de ce soir Une seule solution: la corruption !**
=======================================================================================

.. seealso::

   - https://rabbinchinsky.fr/2020/09/18/corrompre-dieu/

.. contents::
   :depth: 3


Introduction
==============

Je sais que contrairement aux autres années, je ne pourrai pas voir vos
réactions à ces quelques mots que j’ai préparé pour vous et je le regrette.

J’aimerais que nous puissions faire vivre ce collectif qui nous manque
tant d’une autre façon, et j’y travaille.

En attendant, je partage ce texte, que je prononcerai ce soir (plus ou moins).

Vous me connaissez peut-être, vous savez qu’il y aura des variantes,
vous connaissez mes intonations, et vous entendrez, derrière les mots
et les formules, mon désir d’accompagner cette chose si sacrée: notre/votre liberté…

Chana Tova oumétouka

Dracha de Roch hachana 5781: Une seule solution: la corruption
===================================================================

Franchement, je ne vois pas comment nous allons nous en sortir.

Nous ne sommes rien de ce que nous prétendons être, nous prétendons tout
ce qui sert nos intérêts.

La situation accablante me semble en parfaite adéquation avec les
accusations de nos textes. Dans ces conditions, où pourrait donc être
le point de levier du changement ?

Prenons trois exemples
=======================


Un
----

En défendant nos intérêts privés, nous avons créé l’esclavage.
Pour justifier l’esclavage, nous avons inventé le racisme.
Appuyés sur le racisme, nous avons commis, nous commettons toujours,
des génocides.

La ségrégation imposée se retourne parfois en communautarismes, qui
sont alors à leur tour utilisés pour renforcer le racisme et le
fossé social.

Et nous ne prenons toujours pas conscience de ces processus.

Deux
-----

En défendant nos intérêts privés, nous avons créé des écarts sociaux
toujours plus importants, les personnes concentrant le pouvoir dans
le monde aujourd’hui sont si puissantes qu’il est extrêmement dur
de s’y opposer.

Pour justifier cette situation, nous nous racontons que les plus
puissants sont les plus méritants et d’autres fadaises.

Et plus c’est faux, plus nous essayons de nous en convaincre.

Trois
------

En défendant nos intérêts privés, nous cherchons le rendement en
toute chose, et pourtant, nous sommes aveugles à l’investissement
le plus rentable au monde : **la lutte contre la pauvreté**.
Nous préférons idolâtrer notre impuissance à changer les choses.
Nous écartons l’opportunité de gagner de l’argent en sauvant des vies.

**Nous écartons cette opportunité pour protéger ce système**.

Notre faute est trop grande pour que nous la supportions !
===========================================================

Pourquoi alors persévérons-nous ? Justement pour cette raison, parce que
notre faute est trop grande pour que nous la supportions, trop grande
pour que nous ayons le courage de la voir.

En tant qu’humaine, en tant que citoyenne, je suis profondément blessée
par cette stupidité meurtrière qui est la nôtre.

En tant que personne, en ce jour de roch hachana, je suis consciente
aussi de la réalité individuelle de ce phénomène.

Quelles sont les fautes terribles dont je m’accuse ?
Quelles sont les fautes, plus terribles encore, que je fuis, quitte à me
laisser engloutir par les flots, quitte à mourir dans l’antre du bateau
ou dans le ventre du poisson, comme Jonas ?

Et je m’interroge. Ma plus grande faute n’est-elle pas ma peur de la faute ?
Et pourquoi aurais-je si peur ? Serais-je censée échapper à la faute ?
Est-ce une illusion de toute puissance qui me pousse à ces excès ?

Peut-être mais pas forcément. C’est peut-être un sentiment d’urgence à
agir, de surinvestissement de l’action par peur de l’échec, ou d’autres
choses peut-être, qui ne me sont pas encore connues.

A partir de cet instant, je sais que j’ai dix jours, dix petites journées
pour mener l’enquête.

Je n’y arriverai pas.
**Le temps est trop court**, je n’y arriverai pas.
**L’ouvrage est trop grand** et je n’y arriverai pas.
**Je suis trop affaiblie** et je n’y arriverai pas.
Même si **la récompense est grande**, même si le maitre du monde me presse
à l’ouvrage, je n’y arriverai pas.

Et j’en suis réduite à m’appuyer sur la seule solution possible, que me
propose le midrach rabba sur les psaumes : La corruption. (voir texte source en fin de texte).

Car oui, je viens de le découvrir à notre grand soulagement, corrompre
dieu est possible. Écoutons le midrach:

**D’où savons-nous que le Saint, bénie soit-elle, accepte la corruption ?
Parce qu’il est dit dans les proverbes « et la corruption du méchant il
la prend**. Nous sommes gravement fautifs, nous remplissons donc les conditions !

Chers amis, rassemblons-nos ressources pour trouver le bakchich qui seul
nous extirpera de cette situation intenable ! A quel montant s’élève-t-il ?
En quelle monnaie est-il payable ? Le midrach rabba poursuit et nous instruit :

Quelle est la corruption qu’il accepte des fauteurs en ce monde ?
La téchouva (changement d’actes), la téfila (cri et introspection) et la
tsédaka (travail pour la justice sociale, aides financières).
Pour cette raison il est écrit : « de devant toi mon jugement pars ».
Le Saint, bénie soit-elle a dit : « mes enfants, tant que les portes de
la tefila sont ouvertes, faites téchouva, car j’accepte la corruption
dans ce monde ».

Soulagement !

Nous avons dix jours pour rassembler des tefilot. Des tefilot, pas des
« prières ».
On traduit généralement le mot « tefila » par le mot prière, c’est une
erreur. La tefila, c’est l’introspection.
Portée par les textes des offices, certes, par leur sens, par leurs
rythmes, par l’inspiration qui nous vient du collectif, mais l’essentiel
ici, c’est le bilan personnel. Tefila, palal, se juger.
C’est en collectant nos jugements, que nous pourrons rassembler le
trésor nécessaire à la corruption de dieu.

Nos jugements, notre compréhension de nos erreurs, notre esprit critique
sur nous-mêmes.

Nos jugements, notre compréhension du monde, nos idées sur notre action.

Nos jugements sur notre action à titre individuel, mais aussi à titre
collectif. Ai-je été assez forte cette année ? mais aussi, « ai-je été
assez soutenue cette année » et aussi « ai-je suis suffisamment demander
du soutien ? » et « comment aurions-nous pu nous rendre mutuellement plus fort.e ».

Roch hachana est le jour du jugement, il est temps d’exercer ce jugement
que nous avons reçu en partage.

**« mes enfants, tant que les portes de la tefila-jugement sont ouvertes,
faites téchouva, car j’accepte la corruption dans ce monde » nous dit l’Eternelle**.

C’est en rassemblant de l’introspection, des tefilot, que nous pouvons
entrer dans les portails ouverts de la téchouva. Du changement de comportement.

Lorsque les portes s’ouvriront, nous devrons être prêts.

Préparez la monnaie chers amis ! faites les fonds de tiroir ! Participez
à la collecte !

Un peu de conscience tombée derrière le frigo ?
Quelques pièces de perspicacité oubliées au fond d’un tiroir ?
Des mallettes entières d’introspection en petites coupures laissées par
votre grand-mère au grenier ?
Des valises de bonnes idées en lingot amassées par votre grand-oncle ?
Des millions par chèques en blanc d’objection de consciente de votre
voisine, Juste parmi les nations ?
Quelques billets de naïveté bienveillante laissés par vos enfants ?
Une collection de questions dérangeantes posées par vos neveux ?
Un fond de résistance non violente conservé de la période de mai 68 ou
un vieil héritage oublié de 1871 ?
Des héroïsmes juifs, des héroïsmes humains ? Des cœurs brisés ?
Des consciences en alerte ?
Je prends tout, nous en avons besoin ! Toutes les monnaies mentionnées
dans le traité Roch hachana sont acceptées, vous pouvez payer en
AA (Aide à Autrui) en CR (Cri de révolte), CA (Changements dans les actes),
CN (Changement de Nom).

**Le Saint, bénie soit-elle, a dit : « mes enfants, tant que les portes
de la tefila sont ouvertes, faites téchouva, car j’accepte la corruption
dans ce monde. Mais lorsque je siégerai en jugement dans le monde à venir,
je ne prendrai pas de corruption comme il est dit dans les proverbes :
« Il ne se laissera apaiser par aucune rançon ; il se montrera inexorable,
dusses-tu prodiguer les présents. » C’est pour cela que David a dit
« Tu me feras connaître le chemin de la vie, la plénitude des joies
qu’on goûte en ta présence, les délices éternelles [dont on se délecte]
à ta droite. »
Il s’agit des 10 jours de téchouva entre Roch Hachana et Yom kippour»**

Le délai qui nous est imparti n’est pas très clair pour moi.
Avons-nous jusqu’à la fin des temps ?
Avons-nous seulement quelques jours, les 10 jours de téchouva ?
Et de toute façon, je ne suis pas totalement certaine que la date de la
fin des temps ne soit pas avant la fin de la semaine prochaine.

En résumé : une seule stratégie, la corruption, et tout de suite !

Immédiatement, dans la boite prévue à cet effet en sortant de la synagogue,
ou par carte bleue sur internet, faites vos virements dans la monnaie
de votre choix, payez à votre gré, en téchouva, en téfila et/ou en
tsédaka, par promesse de don sur mon site internet, je transférerai
immédiatement. Espérons que cela suffira.

Chana tova oumétouka
