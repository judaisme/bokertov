
.. _bokertov:

=======================================================================================================
🌹🌄🎶💐💗⚖ Boker Tov à toutes et tous ! בוקר טוב ​ par **Rabbin Floriane Chinsky** 🌹🌄🎶💐💗⚖
=======================================================================================================


​🔯🌄🥰🌹👬👭🌻🌈💕💙🙏💫💛💖🕊🐦🌞🎶💐 💗🍀🌺🌄
❣️💖♥️💗​😍💋😚😁🎶💐✡🔯🕎😊😁😘👌​🌻😍🙋⭐☀️
💥⚖️🌷☔👍🍎🍯👨‍👨‍👦‍👨‍👩‍👩‍👦📯🌸
🙆‍♀🙅🙆🕯

.. only:: html

    .. sidebar:: Boker Tov à toutes et tous !

        :Date: |today|
        :Auteure: **Rabbin Floriane Chinksy**
        :Scribe: Noam

        - https://rabbinchinsky.fr/
        - https://judaismeenmouvement.org/les-femmes-et-hommes/floriane-chinsky/
        - https://libertejuive.wordpress.com
        - https://twitter.com/DrRavFlo
        - https://www.youtube.com/c/FlorianeChinsky/videos
        - https://www.youtube.com/user/chirhadach
        - https://twitter.com/JudaismeM/

.. seealso::

   - https://www.youtube.com/c/FlorianeChinsky/videos
   - https://judaismeenmouvement.org/agenda/judaisme-de-demain-revisiter-la-tradition/
   - https://judaismeenmouvement.org/agenda/selihot-surmelin/
   - https://www.youtube.com/playlist?list=PLnHlXjFx9rOQCoscQOKhH81of_tlNAjwu
   - https://judaisme.frama.io/bokertov/
   - http://www.calj.net/
   - https://www.hebcal.com/
   - https://www.youtube.com/playlist?list=PLnHlXjFx9rOSGqqcG_o6UgLyEuaJnnb32
   - https://www.youtube.com/playlist?list=PLnHlXjFx9rOSiCsY-h5WylfXoMWKN8oJQ

.. florianechinsky@gmail.com

.. toctree::
   :maxdepth: 4

   2020/2020
   fetes/fetes
   meta/meta
   index/index
